#!flask/bin/python
from flask import Flask, jsonify
from lxml import html
import requests
from flask_cors import CORS, cross_origin

app = Flask(__name__)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

@app.route('/api/random')
def get_random():
    page = requests.get('http://urbandictionary.com/random.php')
    tree = html.fromstring(page.content)
    word = tree.xpath('//a[@class="word"]/text()')
    meaning = tree.xpath('//div[@class="meaning"]/text()')
    random = [
        {'word': word, 'meaning':meaning}
    ]
    return jsonify(rnd=random)


if __name__ == '__main__':
    app.run(debug=True)
